import 'package:flutter/material.dart';

Row _buildAboutMe(String label, String data) {
  return Row(
    children: [
      Container(
        padding: EdgeInsets.all(10),
        child: Text(
        label,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      Text(
        data,
        style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w400,
        ),
      )
    ]
  );
}

Row _buildContact(IconData icon, String label, String data) {
  return Row(
    children: [
      Icon(icon),
      Container(
        padding: EdgeInsets.all(10),
        child: Text(
        label,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,

          ),
        ),
      ),
      Text(
        data,
        style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w400,
        ),
      )
    ]
  );
}

Column _builSkill(String path) {
  return Column(
    children: [
      Image.asset(path,
      width: 50,
      height: 50,
      )
    ],
  );
}

Row _buildEducation(String label, String data) {
  return Row(
    children: [
      Container(
        padding: EdgeInsets.only(right: 50),
        child: Text(
        label,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      Text(
        data,
        style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
        ),
      )
    ]
  );
}

void main() {
  runApp(
    MyResume()
  );
}

class MyResume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget titleSection = Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: 
                  Text('SARUNPORN CHATUTHAMTHADA', 
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,  
                    ),
                  ),
                ),
                Padding(padding: const EdgeInsets.only(top: 10)),
                Text('Computer Science', 
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.blueGrey[300]
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );

    Widget aboutMeTitleSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(onPressed: null, icon: Icon(Icons.people), iconSize: 35,),
          Text('ABOUT ME', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
        ],
      ),
    );

    Widget aboutMeSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildAboutMe('Nickname :', 'Milk'),
                _buildAboutMe('Gender :', 'Female'),
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildAboutMe('Age :', '21 years old'),
                _buildAboutMe('Birthday :', '4 May 2000'),
              ],
            ),
          )
        ],
      ),
    );

    Widget contactTitleSection = Container(
      padding: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(onPressed: null, icon: Icon(Icons.contact_page), iconSize: 30,),
          Text('CONTACT', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
        ],
      ),
    );

    Widget contactSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            child: Column(
              children: [
                _buildContact(Icons.phone, 'Tel: ', '091-716-8413'),
                _buildContact(Icons.email, 'Email: ', '61160166@go.buu.ac.th'),
              ],
            ),
          )
        ],
      ),
    );

    Widget skillsTitleSection = Container(
      padding: EdgeInsets.only(top: 30, bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(onPressed: null, icon: Icon(Icons.work), iconSize: 30,),
          Text('SKILLS', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
        ],
      ),
    );

    Widget skillSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _builSkill('images/java.png'),
              _builSkill('images/html.png'),
              _builSkill('images/Css.png'),
              _builSkill('images/js.png'),
              _builSkill('images/SwiftUI.png'),
              _builSkill('images/Vue.png')
            ],
          )
        ],
      ),
    );

    Widget educationTitleSection = Container(
      padding: EdgeInsets.only(top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(onPressed: null, icon: Icon(Icons.school), iconSize: 30,),
          Text('EDUCATION', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
        ],
      ),
    );

    Widget educationSection = Container(
      padding: EdgeInsets.all(2),
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 20, top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildEducation(
                  '2018-present',
                  'B.Sc.(Bachelor of Science)\n'
                  'Faculty of Informatics\n'
                  'Burapha University\n'
                  'Long Had Bangsean Road\n'
                  'Chon Buri 20130 '
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 22, bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                 _buildEducation(
                  '2015-2017',
                  'Science-Math\n'
                  'Chonkanyanukoon School \n'
                  '41 Namnaknum Road \n'
                  'Bang Pla Soi, Mueang \n'
                  'Chon Buri 20000 '
                )
              ],
            ),
          )
        ],
      ),
    );


    return MaterialApp(
      title: "Resume",
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My Resume'),
          backgroundColor: Colors.lightBlue[900],
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/milk.JPG',
              width: 900,
              height: 350,
              fit: BoxFit.fitHeight,
            ),
            titleSection,
            aboutMeTitleSection,
            aboutMeSection,
            contactTitleSection,
            contactSection,
            skillsTitleSection,
            skillSection,
            educationTitleSection,
            educationSection,
          ],
        ),
      ),
    );
  }
}